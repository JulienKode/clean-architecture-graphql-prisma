# This is a simple demo of how to use prisma with GraphQL in the clean architecture

## Dependencies

npm install

## Run

npm start

## Test

npm run test

