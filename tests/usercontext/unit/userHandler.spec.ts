// Libraries
import {expect} from 'chai';
// Domain
import {User} from "../../../src/usercontext/domain/entities/user";
import {UserRepository} from "../../../src/usercontext/domain/repositories/userRepository";
import {NotFoundUserError} from "../../../src/usercontext/domain/errors/notFoundUserError";
import {AccountExistWithThisEmailError} from "../../../src/usercontext/domain/errors/accountExistWithThisEmailError";
// Usecases
import {UserBuilder} from "../../../src/usercontext/usecases/userBuilder";
import {UserHandler} from "../../../src/usercontext/usecases/userHandler";
import {FetchAllUser} from "../../../src/usercontext/usecases/queries/fetchAllUser";
import {GetUserById} from "../../../src/usercontext/usecases/queries/getUserById";
import {LoginUser} from "../../../src/usercontext/usecases/queries/loginUser";
import {CreateUser} from "../../../src/usercontext/usecases/queries/createUser";
// Adapters
import {InMemoryUserRepository} from "../../../src/usercontext/adapters/secondaries/inmemory/inMemoryUserRepository";
import {AuthPayload} from "../../../src/usercontext/domain/entities/authPayload";

describe('Users handler', () => {

    let _done: any;

    function createUserHandler(userList: User[] = []): UserHandler {
        const userRepository: UserRepository = new InMemoryUserRepository(userList);
        return new UserHandler(userRepository);
    }

    context('Can fetch a list of users', () => {

        it('should return no users', function (done) {
            _done = done;

            const expectedUserList: User[] = [];
            const userHandler = createUserHandler(expectedUserList);
            const userObserver = userHandler.query<User[]>(new FetchAllUser());

            userObserver.subscribe();
            userObserver.subscribe((users) => {
                expect(users).deep.equal([]);
            }, null, () => done());

        });

        it('should return two users', function (done) {
            _done = done;

            const expectedUserList: User[] = [
                new UserBuilder().withId("1").build(),
                new UserBuilder().withId("2").build()
            ];
            const userHandler = createUserHandler(expectedUserList);
            const userObserver = userHandler.query<User[]>(new FetchAllUser());

            userObserver.subscribe((users) => {
                expect(users).deep.equal(expectedUserList);
            }, null, () => done());

        });

    });

    context("Can get a user with id", () => {

        function getExpectedUsers(): User[] {
            const john = new UserBuilder().withId("1").withFirstName('John').build();
            const danny = new UserBuilder().withId("2").withFirstName('Danny').build();
            return [john, danny]
        }

        it('should get the right user', function (done) {
            _done = done;

            const expectedUsers = getExpectedUsers();
            const userHandler = createUserHandler(expectedUsers);

            const userObserver = userHandler.query<User>(new GetUserById("2"));

            userObserver.subscribe((user) => {
                expect(user).deep.equal(expectedUsers[1]);
            }, null, () => done());

        });

        it('should get error when the user is not found', function (done) {
            _done = done;

            const expectedUsers = getExpectedUsers();
            const userHandler = createUserHandler(expectedUsers);

            const userObserver = userHandler.query<User>(new GetUserById("3"));

            userObserver.subscribe(null, (error => {
                expect(error.message).equal("User 3 not found");
                expect(error).instanceOf(NotFoundUserError);
                done();
            }), null);

        });

    });

    context("Can SignIn and SignUp user", () => {

        const john = new UserBuilder().withId("1").withFirstName('John').withEmail("john@test.com").build();


        it('should login an existing user', function (done) {
            _done = done;

            const userHandler = createUserHandler([john]);
            const userObserver = userHandler.query<AuthPayload>(new LoginUser("john@test.com", "password"));

            userObserver.subscribe((auth) => {
                // expect(auth.user.email).equal("john@test.com");
                expect(auth.user).deep.equal(john);
            }, null, () => done());

        });

        it('should get en error when the user does not exist', function (done) {
            _done = done;

            const userHandler = createUserHandler([]);
            const userObserver = userHandler.query<AuthPayload>(new LoginUser("john@test.com", "password"));

            userObserver.subscribe(null, (error => {
                expect(error.message).equal("User john@test.com not found");
                expect(error).instanceOf(NotFoundUserError);
                done();
            }), null);

        });

        it('should create a new user', function (done) {
            _done = done;

            const userHandler = createUserHandler([]);
            const userObserver = userHandler.query<AuthPayload>(new CreateUser("test@test.com", "password", "John", "Doe"));

            userObserver.subscribe((auth) => {
                expect(auth.user.email).equal("test@test.com");
                expect(auth.user.firstName).equal("John");
                expect(auth.user.lastName).equal("Doe");
            }, null, () => done());

        });

        it('should not create a new user if the email is already registered', function (done) {
            _done = done;

            const userHandler = createUserHandler([john]);
            const userObserver = userHandler.query<AuthPayload>(new CreateUser("john@test.com", "password", "John2", "Doe2"));

            userObserver.subscribe(null, (error => {
                expect(error).instanceOf(AccountExistWithThisEmailError);
                expect(error.email).equal("john@test.com");
                done();
            }), null);

        });

    });

});