import { expect } from 'chai';
import { UserPresenter } from "../../../src/usercontext/adapters/primaries/presenters/userPresenter";
import { UserPresented } from "../../../src/usercontext/adapters/primaries/presenters/userPresented";
import { UserBuilder } from "../../../src/usercontext/usecases/userBuilder";

describe('Users presenter', () => {

    it('Present user resource', () => {
        const user = new UserBuilder()
            .withId("1")
            .withFirstName("John")
            .withLastName("Doe")
            .withEmail("john@example.com")
            .build();

        const userPresented = UserPresenter.present(user);
        const expectedUserPresented: UserPresented = {
            id: "1",
            firstName: "John",
            lastName: "Doe",
            email: "john@example.com"
        };

        expect(userPresented).deep.equal(expectedUserPresented);
    })

});
