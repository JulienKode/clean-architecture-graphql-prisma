import {UserPresented} from "./userPresented";

export interface AuthPayloadPresented {
    token: string
    user: UserPresented
}