export interface UserPresented {
    id: string
    firstName: string
    lastName: string
    email: string
}