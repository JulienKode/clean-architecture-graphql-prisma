import {AuthPayload} from "../../../domain/entities/authPayload";
import {UserPresenter} from "./userPresenter";
import {AuthPayloadPresented} from "./authPayloadPresented";

export class AuthPayloadPresenter {

    static present(auth: AuthPayload): AuthPayloadPresented {
        return {
            token: auth.token,
            user: UserPresenter.present(auth.user),
        }
    }

}