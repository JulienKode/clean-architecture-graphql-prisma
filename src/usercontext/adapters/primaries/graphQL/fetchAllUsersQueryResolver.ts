import {UserHandler} from "../../../usecases/userHandler";
import {UserPresenter} from "../presenters/userPresenter";
import {FetchAllUser} from "../../../usecases/queries/fetchAllUser";
import {User} from "../../../domain/entities/user";
import {map} from "rxjs/operators";

const Query = (userHandler: UserHandler) => ({
   users: () => {
       console.log('here I am');
       return userHandler
           .query<User[]>(new FetchAllUser())
           .pipe(
                map(
                    users => users.map(user => UserPresenter.present(user))
                )
           )
           .toPromise();
   }
});

export const fetchAllUsers = (userHandler: UserHandler) => ({
   Query: Query(userHandler)
});