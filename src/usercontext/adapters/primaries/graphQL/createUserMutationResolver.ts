import {UserHandler} from "../../../usecases/userHandler";
import {map} from "rxjs/operators";
import {CreateUser} from "../../../usecases/command/createUser";
import {AuthPayload} from "../../../domain/entities/authPayload";
import {AuthPayloadPresenter} from "../presenters/authPayloadPresenter";

const Mutation = (userHandler: UserHandler) => ({
    signup: (data, { email, password, firstName, lastName } ) => {
        return userHandler
            .command<AuthPayload>(new CreateUser(email, password, firstName, lastName))
            .pipe(
                map(auth => {
                    console.log("BEFORE");
                    console.log(auth);
                    let res = AuthPayloadPresenter.present(auth);
                    console.log("AFTER");
                    console.log(res);
                    console.log(res.user);
                    console.log(res.user.id);
                    return res;
                })
            )
            .toPromise();
    }
});

export const createUser = (userHandler: UserHandler) => ({
    Mutation: Mutation(userHandler)
});