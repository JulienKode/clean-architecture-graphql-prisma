import {UserHandler} from "../../../usecases/userHandler";
import {AuthPayload} from "../../../domain/entities/authPayload";
import {map} from "rxjs/operators";
import {LoginUser} from "../../../usecases/command/loginUser";
import {AuthPayloadPresenter} from "../presenters/authPayloadPresenter";

const Mutation = (userHandler: UserHandler) => ({
    login: (data, { email, password } ) => {
        return userHandler
            .command<AuthPayload>(new LoginUser(email, password))
            .pipe(
                map(auth => AuthPayloadPresenter.present(auth))
            )
            .toPromise();
    }
});

export const loginUser = (userHandler: UserHandler) => ({
    Mutation: Mutation(userHandler)
});