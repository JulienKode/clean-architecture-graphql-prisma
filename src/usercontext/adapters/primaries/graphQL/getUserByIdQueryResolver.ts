import {UserHandler} from "../../../usecases/userHandler";
import {UserPresenter} from "../presenters/userPresenter";
import {GetUserById} from "../../../usecases/queries/getUserById";
import {User} from "../../../domain/entities/user";
import {map} from "rxjs/operators";

const Query = (userHandler: UserHandler) => ({
    user: (data, { id } ) => {
        return userHandler
            .query<User>(new GetUserById(id))
            .pipe(
                map(user => UserPresenter.present(user))
            )
            .toPromise();
    }
});

export const getUserById = (userHandler: UserHandler) => ({
    Query: Query(userHandler)
});