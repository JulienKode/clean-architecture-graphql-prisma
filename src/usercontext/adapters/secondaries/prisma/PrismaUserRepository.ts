// Libraries
import {Observable, from} from "rxjs";
import {map, flatMap } from "rxjs/operators";
import {Prisma, prisma, UserCreateInput, User as PrismaUser} from "../../../../configuration/prisma/generated/prisma-client";

// Authentification tools
import * as bcrypt from 'bcryptjs'
import * as jwt from 'jsonwebtoken'

//
import {UserRepository} from "../../../domain/repositories/userRepository";
import {User} from "../../../domain/entities/user";
import {AuthPayload} from "../../../domain/entities/authPayload";

export class PrismaUserRepository implements UserRepository {

    all(): Observable<User[]> {
        const res = prisma.users({}).then((users) => {
            return users.map((user) => this.prismaUserToUser(user));
        });
        return from(res);
    }

    get(id: string): Observable<User> {
        const res = prisma.user({id: id}).then((user) => this.prismaUserToUser(user));
        return from(res);
    }

    create(email: string, password: string, firstName: string, lastName: string): Observable<AuthPayload> {
        return from(prisma.user({ email }))
            .pipe(
                map(user => {
                    if (user) throw new Error("Email " + email + " is already use");
                }),
                flatMap(() => from(bcrypt.hash(password, 10))),
                flatMap(encryptedPassword => from(this.createUserAccount(email, encryptedPassword, firstName, lastName))),
                map(user => this.prismaUserToAuthPayload(user))
            );
    }

    login(email: string, password: string): Observable<AuthPayload> {
        return from(prisma.user({ email }))
            .pipe(
                map((user: PrismaUser) => {
                    if (!user) throw new Error("User " + email + " not found");
                    return user;
                }),
                flatMap((user: PrismaUser) => {
                    return from(bcrypt.compare(password, user.password))
                        .pipe(
                            map(valid => {
                                if (!valid) throw new Error('Invalid password');
                                return this.prismaUserToAuthPayload(user);
                            })
                        )
                })
            );
    }

    private createUserAccount(email: string,
                              password: string,
                              firstName: string,
                              lastName: string) {
        return prisma.createUser( { email, password, firstName, lastName } );
    }

    private prismaUserToAuthPayload(user: PrismaUser) {
        const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
        const newUser = this.prismaUserToUser(user);
        return new AuthPayload(token, newUser);
    }

    private prismaUserToUser(user: PrismaUser): User {
        return new User(user.id, user.email, user.firstName, user.lastName)
    }

}