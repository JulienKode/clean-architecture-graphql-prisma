import {UserRepository} from "../../../domain/repositories/userRepository";
import {User} from "../../../domain/entities/user";
import {filter, head, propEq} from 'ramda';
import {Observable, of, throwError} from 'rxjs';
import {AuthPayload} from "../../../domain/entities/authPayload";

export class InMemoryUserRepository implements UserRepository {

    constructor(private _users: User[] = []) {
    }

    all(): Observable<User[]> {
        return of(this._users);
    }

    get(id: string): Observable<User> {
        const user = head(filter(propEq('id', id), this._users));
        return of(user);
    }

    create(email: string, password: string, firstName: string, lastName: string): Observable<AuthPayload> {
        const id = Math.random().toString(36).substring(12);
        const user = new User(id, firstName, lastName, email);
        if (this.findUserWith(email) !== undefined)
            return throwError("Email " + email + " is already use");
        this._users.push(user);
        return of(new AuthPayload("", user));
    }

    login(email: string, password: string): Observable<AuthPayload> {
        let user = this.findUserWith(email);
        if (user === undefined)
            return throwError("User " + email + " not found");
        return of(new AuthPayload("", user));
    }

    /**
     * @private
     * @method findUserWith
     * Find a user inside this in memory repository
     *
     * @param email: of the user you want to find
     *
     * @return the user find in the database or undefined if it does not exist
    **/
    private findUserWith(email: string): User | undefined {
        return this._users.find(item => item.email === email);
    }

}