import {UserRepository} from "../domain/repositories/userRepository";
import {UserBuilder} from "../usecases/userBuilder";
import {InMemoryUserRepository} from "../adapters/secondaries/inmemory/inMemoryUserRepository";
import {PrismaUserRepository} from "../adapters/secondaries/prisma/PrismaUserRepository";

export class UserContextDependencyFactory {

    static userRepository(): UserRepository {
        switch (process.env.REPOSITORY) {
            default:
                return new PrismaUserRepository();
            case "inmemory":
                const john = new UserBuilder()
                    .withId('1')
                    .withEmail('john@doe.com')
                    .withFirstName('John')
                    .withLastName('Doe')
                    .build();

                const danny = new UserBuilder()
                    .withId('2')
                    .withEmail('danny@doe.com')
                    .withFirstName('Danny')
                    .withLastName('Danone')
                    .build();

                const chris = new UserBuilder()
                    .withId('3')
                    .withEmail('chris@example.com')
                    .withFirstName('Chris')
                    .withLastName('Example')
                    .build();

                return new InMemoryUserRepository([ john, danny, chris ]);
        }
    }
}