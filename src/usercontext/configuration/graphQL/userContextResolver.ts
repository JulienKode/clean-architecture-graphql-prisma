import { merge }  from "lodash";
import {userContextDI} from "../userContextDI";
import {fetchAllUsers} from "../../adapters/primaries/graphQL/fetchAllUsersQueryResolver";
import {getUserById} from "../../adapters/primaries/graphQL/getUserByIdQueryResolver";
import {createUser} from "../../adapters/primaries/graphQL/createUserMutationResolver";
import {loginUser} from "../../adapters/primaries/graphQL/loginUserMutationResolver";

export const UserContextResolver = merge({},
    fetchAllUsers(userContextDI.userHandler),
    getUserById(userContextDI.userHandler),
    createUser(userContextDI.userHandler),
    loginUser(userContextDI.userHandler)
);