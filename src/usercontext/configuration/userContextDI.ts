import {UserRepository} from "../domain/repositories/userRepository";
import {UserContextDependencyFactory} from "./userContextDependencyFactory";
import {UserHandler} from "../usecases/userHandler";

const userRepository: UserRepository = UserContextDependencyFactory.userRepository();
const userHandler: UserHandler = new UserHandler(userRepository);

export const userContextDI = {
    userHandler
};