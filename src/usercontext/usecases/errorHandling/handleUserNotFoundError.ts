import {User} from "../../domain/entities/user";
import {NotFoundUserError} from "../../domain/errors/notFoundUserError";

export function handleUserNotFoundError(user: User, item: string): User {
    if (user === undefined) {
        throw new NotFoundUserError("User " + item + " not found");
    }
    return user
}
