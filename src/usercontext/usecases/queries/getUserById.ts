import {QueryHandler} from "../../domain/handler/queryHandler";
import {UserRepository} from "../../domain/repositories/userRepository";
import {User} from "../../domain/entities/user";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {handleUserNotFoundError} from "../errorHandling/handleUserNotFoundError";

export class GetUserById implements QueryHandler<User> {
    constructor(private _id: string) {
    }

    query(userRepository: UserRepository): Observable<User> {
        return userRepository
            .get(this._id)
            .pipe(
                map(user => handleUserNotFoundError(user, this._id))
            );
    }

}