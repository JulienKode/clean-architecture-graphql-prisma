import { UserRepository } from "../../domain/repositories/userRepository";
import { QueryHandler } from "../../domain/handler/queryHandler";
import {User} from "../../domain/entities/user";
import {Observable} from "rxjs";

export class FetchAllUser implements QueryHandler<User[]> {

    query(userRepository: UserRepository): Observable<User[]> {
        return userRepository.all();
    }

}