import {UserRepository} from "../domain/repositories/userRepository";
import {QueryHandler} from "../domain/handler/queryHandler";
import {User} from "../domain/entities/user";
import {Observable} from "rxjs";
import {CommandHandler} from "../domain/handler/commandHandler";

export class UserHandler {

    constructor(private  _userRepository: UserRepository) {
    }

    query<T>(handler: QueryHandler<T>): Observable<T> {
        return handler.query(this._userRepository);
    }

    command<T>(handler: CommandHandler<T>): Observable<T> {
        return handler.command(this._userRepository);
    }

}