import { User } from "../domain/entities/user";

export class UserBuilder {

    protected _id: string;
    protected _firstName: string;
    protected _lastName: string;
    protected _email: string;

    withId(id: string): UserBuilder {
        this._id = id;
        return this;
    }

    withFirstName(firstName: string): UserBuilder {
        this._firstName = firstName;
        return this;
    }

    withLastName(lastName: string): UserBuilder {
        this._lastName = lastName;
        return this;
    }


    withEmail(email: string) {
        this._email = email;
        return this;
    }

    build(): User {
        return new User(this._id, this._firstName, this._lastName, this._email);
    }

}
