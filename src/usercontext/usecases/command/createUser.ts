import {CommandHandler} from "../../domain/handler/commandHandler";
import {UserRepository} from "../../domain/repositories/userRepository";
import {Observable, of} from "rxjs";
import {AuthPayload} from "../../domain/entities/authPayload";

export class CreateUser implements CommandHandler<AuthPayload> {

    constructor(private _email: string,
                private _password: string,
                private _firstName: string,
                private _lastName: string) {
    }

    command(userRepository: UserRepository): Observable<AuthPayload> {
        return userRepository
            .create(this._email, this._password, this._firstName, this._lastName);
    }

}