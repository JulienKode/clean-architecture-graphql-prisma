import {CommandHandler} from "../../domain/handler/commandHandler";
import {UserRepository} from "../../domain/repositories/userRepository";
import {Observable} from "rxjs";
import {AuthPayload} from "../../domain/entities/authPayload";

export class LoginUser implements CommandHandler<AuthPayload> {
    constructor(private _email: string, private _password: string) {
    }

    command(userRepository: UserRepository): Observable<AuthPayload> {
        return userRepository
            .login(this._email, this._password);
    }

}