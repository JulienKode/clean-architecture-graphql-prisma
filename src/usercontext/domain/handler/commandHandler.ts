import { UserRepository } from "../repositories/userRepository";
import {Observable} from "rxjs";

export interface CommandHandler<T> {

    command(userRepository: UserRepository): Observable<T>

}