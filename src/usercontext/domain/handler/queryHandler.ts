import { UserRepository } from "../repositories/userRepository";
import {Observable} from "rxjs";

export interface QueryHandler<T> {

    query(userRepository: UserRepository): Observable<T>

}