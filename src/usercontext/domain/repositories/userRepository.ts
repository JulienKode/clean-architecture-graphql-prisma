import {User} from "../entities/user";
import {Observable} from "rxjs";
import {AuthPayload} from "../entities/authPayload";

export interface UserRepository {

    all(): Observable<User[]>

    get(id: string): Observable<User>

    login(email: string, password: string): Observable<AuthPayload>

    create(email: string, password: string, firstName: string, lastName: string): Observable<AuthPayload>

}