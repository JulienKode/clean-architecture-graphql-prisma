export class User {

    constructor(private _id: string,
                private _firstName: string,
                private _lastName: string,
                private _email: string) {
    }

    get id(): string {
        return this._id;
    }

    get firstName(): string {
        return this._firstName;
    }

    get lastName(): string {
        return this._lastName;
    }

    get email(): string {
        return this._email;
    }

}
