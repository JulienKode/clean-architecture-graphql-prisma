import {User} from "./user";

export class AuthPayload {

    constructor(private _token: string,
                private _user: User) {
    }

    get user(): User {
        return this._user;
    }

    get token(): string {
        return this._token;
    }

}
