import './app.ts';
import {rootGraphQLResolver} from "./configuration/graphQL/rootResolver";
const { GraphQLServer } = require('graphql-yoga');

const server = new GraphQLServer({
    typeDefs: './src/configuration/graphQL/rootSchema.graphql',
    resolvers: rootGraphQLResolver
});

server.start(() => console.log('Server is running on http://localhost:4000'));