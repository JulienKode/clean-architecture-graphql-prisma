import { merge }  from 'lodash';
import {UserContextResolver} from "../../usercontext/configuration/graphQL/userContextResolver";

export const rootGraphQLResolver = merge({},
    UserContextResolver
);